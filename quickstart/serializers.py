from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Perfil


class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('url', 'username', 'email', 'groups',)


class GroupSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Group
		fields = ('url', 'name',)

# class PersonSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		print ('esta chingadera no llega modelosss')
# 		model = Perfil
# 		print (model)
# 		fields = ('name','fecha','puesto',)
class PerfilSerializer(serializers.ModelSerializer):
		model = Perfil
		filter = ('name',)